import UserType from "./UserType";

type ArticleType = {
  category: string;
  id: string;
  title: string;
  body: string;
  author: UserType;
  date: string;
};

export default ArticleType;
