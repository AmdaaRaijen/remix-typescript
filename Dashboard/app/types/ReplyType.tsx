import UserType from "./UserType";

type ReplyType = {
  id: string;
  user_id: string;
  message: string;
  author: UserType;
};

export default ReplyType;
