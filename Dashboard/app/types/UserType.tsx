type UserType = {
  id: string;
  name: string;
  username: string;
  password: string;
};

export default UserType;
