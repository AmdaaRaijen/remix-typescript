import UserType from "./UserType";
type CommentType = {
  id: string;
  article_id: string;
  name: string;
  author: UserType;
  message: string;
};

export default CommentType;
