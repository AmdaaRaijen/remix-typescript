import { useEffect, useState } from "react";

const Body = () => {
  const [value, setValue] = useState("");
  return (
    <div className="mt-8">
      <textarea
        name="body"
        className="textarea  border-2 border-slate-400 w-full h-[40vh]"
        placeholder="Mulai tulis disini..."
      ></textarea>
    </div>
  );
};

export default Body;
