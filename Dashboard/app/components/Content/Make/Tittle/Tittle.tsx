import React from "react";

const Tittle = () => {
  return (
    <div className="mt-20 flex flex-col gap-4">
      <div className="flex gap-5 items-center">
        <label className="font-semibold">Kategori</label>
        <select
          name="category"
          className="w-[12.6rem] border bg-[#FAF9FF] border-b-2 border-[#FAF9FF] border-b-slate-400 text-slate-400 outline-none"
        >
          <option disabled selected>
            Pilih Kategori
          </option>
          <option>Technology</option>
          <option>Lifestyle</option>
          <option>Otomotif</option>
        </select>
      </div>
      <div className="flex gap-11 items-center">
        <label className="font-semibold">Judul</label>
        <input
          name="title"
          type="text"
          className="bg-[#FAF9FF] w-full border-b-2 border-slate-400 outline-none"
          placeholder="Tulis Judul Artikelmu"
        />
      </div>
    </div>
  );
};

export default Tittle;
