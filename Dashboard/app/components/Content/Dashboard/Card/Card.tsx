import { FiEye, FiUserPlus } from "react-icons/fi";
import { FaEye } from "react-icons/fa";

const Card = () => {
  return (
    <div className="bg-white p-3 h-36 w-52 border-2 rounded-3xl mt-10">
      <div className="flex justify-between text-slate-400">
        <p className="text-sm text-slate-400">Total Penonton</p>
        <FiUserPlus />
      </div>
      <div className="flex items-center justify-center gap-6 pt-7">
        <FaEye className="w-10 h-10 text-slate-400" />
        <p className="text-4xl font-bold">10</p>
      </div>
    </div>
  );
};

export default Card;
