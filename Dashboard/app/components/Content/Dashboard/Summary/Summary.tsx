import { useEffect, useState } from "react";
import ArticleType from "~/types/ArticleType";

const Summary = () => {
  const [summary, setSummary] = useState<ArticleType[]>([]);
  async function handleFetchArticles() {
    const res = await fetch("http://localhost:5000/articles");
    if (res.ok) {
      const articlesJson = await res.json();
      const articlesData: ArticleType[] = articlesJson.map((article: any) => {
        return {
          ...article,
        };
      });
      setSummary(articlesData);
      console.log(articlesData);
    }
  }

  function breakLongSentence(sentence: string, limit: number): any {
    if (sentence.length > limit) {
      return <p> `${sentence.substring(0, limit)}....`</p>;
    }
    return sentence;
  }

  useEffect(() => {
    handleFetchArticles();
  }, []);

  return (
    <div className="mt-8 min-w-[70rem] w-fit">
      <h2 className="text-xl font-medium">Ringkasan Artikel</h2>
      <div className="mt-4">
        <div className="overflow-x-auto rounded-xl border-2 w-[75rem]">
          <table className="table w-full">
            <tbody className="font-medium w-full ">
              {summary.map((data, idx) => {
                return (
                  <tr className="border-b-2 border-b-white">
                    <th>{idx + 1}.</th>
                    <td className="">{breakLongSentence(data.title, 80)}</td>
                    <td>
                      <p className="text-[#8886FF]  opacity-[71%]">
                        {data.category}
                      </p>
                    </td>
                    <td>
                      <p className="text-slate-500">2x dibaca</p>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
};

export default Summary;
