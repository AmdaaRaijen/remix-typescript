import React from "react";

const Welcome = () => {
  return (
    <div>
      <p className="text-xl font-bold">DashBoard</p>
      <p className="font-semibold text-slate-400 text-sm">
        Welcome Back, Bintang
      </p>
    </div>
  );
};

export default Welcome;
