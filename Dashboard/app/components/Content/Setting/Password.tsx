import React from "react";

const Password = () => {
  return (
    <div className="mt-20">
      <table>
        <tr className="">
          <td className="w-[10rem] py-3">
            <label className="font-medium">Ubah sandi</label>
          </td>
          <td>
            <input
              type="text"
              className="bg-[#FAF9FF] outline-none border-b-2 border-slate-400 w-72"
            />
          </td>
        </tr>
        <tr className="">
          <td className="w-[10rem] py-3">
            <label className="font-medium">Tulis ulang sandi</label>
          </td>
          <td>
            <input
              type="text"
              className="bg-[#FAF9FF] outline-none border-b-2 border-slate-400 w-72"
            />
          </td>
        </tr>
      </table>
    </div>
  );
};

export default Password;
