import { useState, useEffect } from "react";

const PersonalData = () => {
  const [user, setUser] = useState<string | null>("");
  const [name, setName] = useState<string | null>("");
  const [uId, setUId] = useState<string | null>("");

  useEffect(() => {
    const getUser: string | null = window.localStorage.getItem("user");
    const getName: string | null = window.localStorage.getItem("name");

    setUser(getUser);
    setName(getName);
  }, []);

  return (
    <div className="mt-20 flex flex-col gap-5">
      <table>
        <tr className="">
          <td className="w-[10rem] py-3">
            <label className="font-medium">Nama</label>
          </td>
          <td>
            <input
              type="text"
              className="bg-[#FAF9FF] outline-none border-b-2 border-slate-400 w-72"
              placeholder={user ? user : "your user name"}
            />
          </td>
        </tr>
        <tr className="">
          <td className="max-w-[10rem] py-3">
            <label className="font-medium">Email</label>
          </td>
          <td>
            <input
              type="text"
              className="bg-[#FAF9FF] outline-none border-b-2 border-slate-400 w-72"
              placeholder={name ? name : "your name"}
            />
          </td>
        </tr>
        <tr className="">
          <td className="max-w-[10rem] py-3">
            <label className="font-medium">No. Telpon</label>
          </td>
          <td>
            <input
              type="text"
              className="bg-[#FAF9FF] outline-none border-b-2 border-slate-400 w-72"
              placeholder="081234567891"
            />
          </td>
        </tr>
      </table>
    </div>
  );
};
export default PersonalData;
