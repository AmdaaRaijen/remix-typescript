import { useEffect, useState } from "react";
import Cookies from "universal-cookie";
const Header = () => {
  const [user, setUser] = useState<string | null>("");
  const [name, setName] = useState<string | null>("");
  const cookie = new Cookies();
  useEffect(() => {
    const getUser: string | null = cookie.get("user");
    const getName: string | null = cookie.get("name");

    setUser(getUser);
    setName(getName);
  }, []);

  return (
    <header className="sticky w-full bg-white p-7 flex justify-between items-center">
      <div className="flex items-center gap-2">
        <p className="font-medium text-slate-600">Performa Artikel</p>
      </div>
      <div className="flex items-center gap-3 mr-6">
        <div className="h-fit p-5 rounded-full bg-slate-600"></div>
        <section className="text-sm">
          <p className="font-medium">{user}</p>
          <p className="text-slate-400 text-xs">{name}</p>
        </section>
      </div>
    </header>
  );
};

export default Header;
