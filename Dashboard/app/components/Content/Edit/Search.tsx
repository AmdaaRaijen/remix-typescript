import React from "react";
import { BiSearch } from "react-icons/bi";

const Search = () => {
  return (
    <div className="mt-4">
      <div className=" border w-fit rounded-3xl flex gap-2 items-center p-3 border-slate-500">
        <input
          type="text"
          className="outline-none bg-[#FAF9FF] font-medium text-slate-700"
          placeholder="Cari"
        />
        <BiSearch className="text-slate-500" />
      </div>
    </div>
  );
};

export default Search;
