import React, { useEffect, useState } from "react";
import { BsFillTrashFill } from "react-icons/bs";
import { FaEdit } from "react-icons/fa";
import ArticleType from "~/types/ArticleType";
import CardEdit from "./CardEdit";

const Edit = () => {
  const [summary, setSummary] = useState<ArticleType[]>([]);
  async function handleFetchArticles() {
    const res = await fetch("http://localhost:5000/articles");
    if (res.ok) {
      const articlesJson = await res.json();
      const articlesData: ArticleType[] = articlesJson.map((article: any) => {
        return {
          ...article,
        };
      });
      setSummary(articlesData);
    }
  }

  useEffect(() => {
    handleFetchArticles();
  }, []);

  async function handleDeleteArticle(id: string) {
    const res = await fetch(`http://localhost:5000/articles/${id}`, {
      method: "DELETE",
    });
    if (res.ok) {
      const filterSummary = summary.filter((data) => data.id !== id);
      setSummary(filterSummary);
    }
  }
  return (
    <div className="mt-8   w-fit">
      <div className="mt-4">
        <div className="rounded-xl border-2 ">
          <table className="table w-[75rem]">
            <tbody className="font-medium w-[80rem]">
              {summary.map((data, idx) => {
                return (
                  <CardEdit
                    handleDelete={handleDeleteArticle}
                    summaryData={data}
                    idx={idx}
                  />
                );
              })}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
};

export default Edit;
