import { useEffect, useState } from "react";
import { BsFillTrashFill } from "react-icons/bs";
import { FaEdit } from "react-icons/fa";
import ArticleType from "~/types/ArticleType";

interface CardEditProps {
  summaryData: ArticleType;
  idx: number;
  handleDelete: (id: string) => void;
}

const CardEdit = ({ summaryData, idx, handleDelete }: CardEditProps) => {
  function breakLongSentence(sentence: string, limit: number): any {
    if (sentence.length > limit) {
      return (
        <p>
          {sentence.substring(0, limit)}{" "}
          <span className="text-blue-500"> ....</span>
        </p>
      );
    }
    return sentence;
  }

  return (
    <tr key={summaryData.id} className=" border-b-2 border-b-slate-200 ">
      <th>{idx + 1}.</th>
      <td>
        <p className="hover:underline cursor-pointer w-fit">
          {breakLongSentence(summaryData.title, 80)}
        </p>
      </td>
      <td>
        <div className="flex flex-col items-center ">
          <button className="btn flex flex-col items-center bg-blue-500 p-1 w-16 rounded-md border-none">
            <FaEdit className="text-white" />
            <p className="text-white text-xs font-semibold">Edit</p>
          </button>
        </div>
      </td>
      <td>
        <div className="flex flex-col items-center ">
          <button
            onClick={() => handleDelete(summaryData.id)}
            className="btn flex flex-col items-center bg-red-500 p-1 w-16 rounded-md border-none"
          >
            <BsFillTrashFill className="text-white" />
            <p className="text-white text-xs font-semibold">Hapus</p>
          </button>
        </div>
      </td>
    </tr>
  );
};

export default CardEdit;
