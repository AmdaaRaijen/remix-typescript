import { useState } from "react";
import { AiFillLike } from "react-icons/ai";

interface Props {
  message: string;
  author: string;
  parent?: Boolean;
  handleShowReply?: () => Promise<void>;
  color: string;
}
const CommentItem = ({
  message,
  author,
  color,
  parent = false,
  handleShowReply,
}: Props) => {
  const [like, setLike] = useState<Boolean>(false);

  return (
    <div>
      <section className="flex items-start gap-4 ">
        <div className={`p-4 rounded-full w-fit h-fit ${color} flex`}></div>
        <div className=" w-full flex flex-col">
          <div>
            <div className=" w-full flex flex-col p-2  gap-2 items-start bg-slate-100 text-black text-sm rounded-md  shadow-sm">
              <p className="font-semibold">{author}</p>
              <p>{message}</p>
            </div>
            <section className="flex items-center gap-2 text-sm font-medium text-slate-400 mt-1 ">
              <button
                onClick={() => setLike(!like)}
                className={`btn ${
                  like ? "text-blue-500" : ""
                }  btn-ghost btn-xs`}
              >
                suka
              </button>

              {like && (
                <>
                  <div className="p-1 rounded-full border bg-[#5A57FF] w-5 h-5 flex items-center">
                    <AiFillLike className="text-[#ebebff]" />
                  </div>
                  <p>1</p>
                </>
              )}
              <p>|</p>
              <button className="btn btn-ghost btn-xs">balas</button>
              {parent && handleShowReply && (
                <button
                  onClick={() => handleShowReply()}
                  className="hover:underline"
                >
                  Lihat balasan
                </button>
              )}
            </section>
          </div>
        </div>
      </section>
    </div>
  );
};

export default CommentItem;
