import React, { useState } from "react";
import CommentItem from "../CommentItem/CommentItem";
import ReplyType from "~/types/ReplyType";

interface Props {
  message: string;
  author: string;
  color?: string;
}

export default function CommentWrapper({
  message,
  author,
  color = "bg-blue-400",
}: Props) {
  const [showReplies, setShowReplies] = useState<Boolean>(false);
  const [replies, setReplies] = useState<ReplyType[]>([]);

  async function handleShowReply() {
    if (showReplies) return;
    const resReplies = await fetch("http://localhost:5000/replies");
    const resUsers = await fetch("http://localhost:5000/users");

    if (resReplies.ok && resUsers.ok) {
      const dataReplies = await resReplies.json();
      const dataUsers = await resUsers.json();

      const replies = dataReplies.map((reply: ReplyType) => {
        const author = dataUsers.find((user: any) => user.id === reply.user_id);
        return {
          ...reply,
          author,
        };
      });

      setReplies(replies);
      setShowReplies(true);
    }
  }

  return (
    <div>
      <CommentItem
        message={message}
        author={author}
        color={color}
        parent={true}
        handleShowReply={handleShowReply}
      />
      {replies?.length > 0 && showReplies && (
        <div className="ml-12 mt-1">
          {replies.map((reply, index) => (
            <CommentItem
              key={index}
              message={reply.message}
              author={reply.author.name}
              color="bg-blue-400"
              handleShowReply={handleShowReply}
            />
          ))}
        </div>
      )}
    </div>
  );
}
