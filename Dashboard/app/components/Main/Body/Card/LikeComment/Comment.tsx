import { MdOutlineAddPhotoAlternate } from "react-icons/md";
import { IoMdArrowDropdown } from "react-icons/io";
import { IoRocketOutline, IoTimeOutline } from "react-icons/io5";
import CommentWrapper from "./CommentWrapper/CommentWrapper";
import CommentType from "~/types/CommentType";

interface Props {
  comments: CommentType[];
}

const Comment = ({ comments }: Props) => {
  return (
    <div>
      <div className="flex items-center gap-2 my-1">
        <div className="p-4 rounded-full w-fit h-fit bg-blue-400"></div>
        <div className="flex p-2 border-2 w-full rounded-2xl items-center ">
          <input
            type="text"
            className="w-[90%] outline-none"
            placeholder="Tambah komentar ..."
          />
          <input
            type="file"
            hidden
            accept="image/*"
            name="image"
            id="comment-img"
          />
          <label htmlFor="comment-img">
            <MdOutlineAddPhotoAlternate className="text-slate-400 w-6 h-6 cursor-pointer hover:text-slate-700 duration-200" />
          </label>
        </div>
      </div>
      <div className="dropdown dropdown-bottom text-slate-500 font-medium text-sm mb-4">
        <div tabIndex={0} className="flex items-center">
          <label className="">Paling relevan</label>
          <IoMdArrowDropdown className="w-5 h-5" />
        </div>
        <ul
          tabIndex={0}
          className="dropdown-content menu p-2 shadow bg-base-100 rounded-box w-72"
        >
          <li className=" text-xs">
            <div>
              <IoRocketOutline className="w-5 h-5" />
              <div className="flex flex-col">
                <p>Paling Relevan</p>
                <p className="font-light">Lihat komentar paling relevan</p>
              </div>
            </div>
          </li>
          <li className=" text-xs">
            <div>
              <IoTimeOutline className="w-5 h-5" />
              <div className="flex flex-col">
                <p>Terbaru</p>
                <p className="font-light">Lihat komentar terbaru</p>
              </div>
            </div>
          </li>
        </ul>
      </div>
      <div className="space-y-5">
        {comments.map((comment) => (
          <CommentWrapper
            key={comment.id}
            message={comment.message}
            author={comment.author.name}
          />
        ))}
      </div>
    </div>
  );
};

export default Comment;
