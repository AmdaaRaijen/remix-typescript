import React, { useState } from "react";
import {
  AiFillLike,
  AiOutlineShareAlt,
  AiOutlineComment,
} from "react-icons/ai";

interface Props {
  setComment: React.Dispatch<React.SetStateAction<Boolean>>;
  handleFetchComments: () => Promise<void>;
}

const LikeComment = ({ setComment, handleFetchComments }: Props) => {
  const [like, setLike] = useState<Boolean>(false);

  return (
    <div className="mt-2 text-xs text-slate-500">
      <div className="flex justify-between ">
        <div className="flex items-center gap-1">
          <div className="p-1 rounded-full border bg-[#5A57FF] w-5 h-5 flex items-center">
            <AiFillLike className="text-[#ebebff]" />
          </div>
          <div className="hover:text-blue-500 hover:underline cursor-pointer">
            <p>Putra Pradana dan 3 lainnya</p>
          </div>
        </div>
        <div className="flex gap-2 items-center ">
          <button
            onClick={() => {
              handleFetchComments();
              setComment(true);
            }}
            className="hover:text-blue-500 hover:underline cursor-pointer"
          >
            3 Komentar
          </button>
          <span>|</span>
          <p className="hover:text-blue-500 hover:underline cursor-pointer">
            1 kali dibagikan
          </p>
        </div>
      </div>
      <hr className="mt-2 mb-3" />
      <div className="flex justify-between text-base font-semibold text-slate-500 ">
        <div
          id="tooltip-like"
          onClick={() => setLike(!like)}
          className={`flex items-center gap-1 duration-100 cursor-pointer p-2 w-full justify-center  rounded-md hover:bg-slate-100   ${
            like ? "text-blue-500" : ""
          }`}
        >
          <AiFillLike className="w-6 h-6  " />
          <button>Suka</button>
        </div>
        <div
          onClick={() => {
            handleFetchComments();
            setComment(true);
          }}
          className="flex items-center gap-1 cursor-pointer p-2 w-full justify-center  rounded-md hover:bg-slate-100 duration-100"
        >
          <AiOutlineComment className="w-6 h-6" />
          <button>Komentar</button>
        </div>
        <div className="flex items-center gap-1 cursor-pointer p-2 w-full justify-center  rounded-md hover:bg-slate-100 duration-100">
          <AiOutlineShareAlt className="w-6 h-6" />
          <button>Bagikan</button>
        </div>
      </div>
    </div>
  );
};

export default LikeComment;
