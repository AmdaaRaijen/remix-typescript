import { useState, useEffect } from "react";
import Comment from "./LikeComment/Comment";
import LikeComment from "./LikeComment/LikeComment";
import { AiOutlinePlus } from "react-icons/ai";
import ArticleType from "~/types/ArticleType";
import parseDate from "~/utils/parseDate";
import UserType from "~/types/UserType";
import CommentType from "~/types/CommentType";

interface Props {
  article: ArticleType;
}

const Card = ({ article }: Props) => {
  const [showComment, setShowComment] = useState<Boolean>(false);
  const [comments, setComments] = useState<CommentType[]>([]);

  async function handleFetchComments() {
    if (showComment) return;
    const resComment = await fetch("http://localhost:5000/comments");
    const resUser = await fetch("http://localhost:5000/users");
    if (resUser.ok && resComment.ok) {
      const commentJson = await resComment.json();
      const usersJson: UserType[] = await resUser.json();
      const commentsData: CommentType[] = commentJson.map((comment: any) => {
        const author = usersJson.find((user) => user.id === comment.user_id);
        return {
          ...comment,
          author,
        };
      });
      setComments(commentsData);
    }
  }

  return (
    <div className="w-[30rem] h-fit bg-white flex flex-col p-4 rounded-xl  border">
      <div className="h-fit">
        <div className="flex justify-between">
          <div className="flex items-center gap-2">
            <div className="p-4  rounded-full bg-slate-400 w-fit h-fit"></div>
            <div className="flex flex-col">
              <p className="font-semibold">{article.author.name}</p>
              <p className="text-xs text-slate-500">
                {parseDate(article.date)}
              </p>
            </div>
          </div>
          <div className="flex items-center gap-1 font-semibold text-[#5A57FF] duration-200 cursor-pointer pr-2 pl-2  rounded-md hover:bg-[#ebebff]">
            <AiOutlinePlus className=" w-4 h-4" />
            <p>Ikuti</p>
          </div>
        </div>
      </div>
      <div className="mt-6 break-words ">
        <h2 className="font-medium text-xl mb-2 ">{article.title}</h2>
        <p className="text-justify mb-1 text-sm">{article.body}</p>
      </div>

      <LikeComment
        setComment={setShowComment}
        handleFetchComments={handleFetchComments}
      />
      {showComment && comments.length ? <Comment comments={comments} /> : null}
    </div>
  );
};

export default Card;
