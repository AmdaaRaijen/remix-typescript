import { BiSearchAlt2 } from "react-icons/bi";
import { NavLink } from "react-router-dom";
import {
  AiOutlineHome,
  AiOutlineDashboard,
  AiOutlineUser,
} from "react-icons/ai";

const Navbar = () => {
  return (
    <div className="bg-white sticky top-0 p-2 pl-52 pr-52 shadow-sm">
      <div className="flex items-center gap-3 justify-between">
        <div className="flex items-center gap-2">
          <p className="p-[16px] bg-[#5A57FF] w-2 h-2 rounded-sm flex items-center justify-center text-white">
            B
          </p>
          <div className="flex gap-2 items-center  bg-slate-100 pr-2 pl-2 rounded-[0.25rem] pt-1 pb-1">
            <BiSearchAlt2 />
            <input
              type="text"
              className="outline-none bg-slate-100"
              placeholder="Cari"
            />
          </div>
        </div>
        <div className="flex  gap-5 text-slate-500">
          <NavLink
            to="/"
            className="hover:text-slate-700 flex flex-col items-center"
          >
            <AiOutlineHome className="w-6 h-6" />
            <p className="text-xs">Home</p>
          </NavLink>
          <NavLink
            to="/dashboard"
            className="hover:text-slate-700 flex flex-col items-center"
          >
            <AiOutlineDashboard className="w-6 h-6" />
            <p className="text-xs">Dashboard</p>
          </NavLink>
          <NavLink
            to="/dashboard/setting"
            className="hover:text-slate-700 flex flex-col items-center"
          >
            <AiOutlineUser className="w-6 h-6" />
            <p className="text-xs">Saya</p>
          </NavLink>
        </div>
      </div>
    </div>
  );
};

export default Navbar;
