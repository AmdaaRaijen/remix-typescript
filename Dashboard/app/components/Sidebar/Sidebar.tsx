import { RxDashboard } from "react-icons/rx";
import { RiPagesLine } from "react-icons/ri";
import { TfiWrite } from "react-icons/tfi";
import { FiLogOut, FiSettings, FiEdit } from "react-icons/fi";
import { NavLink } from "@remix-run/react";

const Navbar = () => {
  const activeStyle = {
    color: "white",
  };
  const inactiveStyle = {
    color: "#e2e8f0",
  };

  return (
    <div className="flex sticky flex-col justify-between p-8 pl-10 bg-[#5A57FF] h-screen text-white rounded-r-3xl">
      <div className="flex flex-col">
        <div className="text-xl font-bold mt-6">
          <h2>Dashboard</h2>
        </div>
        <div className="font-[200] mt-20 flex flex-col gap-6">
          <NavLink
            style={({ isActive }) => (isActive ? activeStyle : inactiveStyle)}
            to="/dashboard"
          >
            <div className="flex items-center font-normal hover:text-white  gap-2">
              <RxDashboard />
              <p>Dashboard</p>
            </div>
          </NavLink>
          <NavLink
            style={({ isActive }) => (isActive ? activeStyle : inactiveStyle)}
            to="/dashboard/tulis"
          >
            <div className="flex items-center font-normal hover:text-white  gap-2">
              <TfiWrite />
              <p>Buat</p>
            </div>
          </NavLink>
          <NavLink
            style={({ isActive }) => (isActive ? activeStyle : inactiveStyle)}
            to="/dashboard/edit"
          >
            <div className="flex items-center font-normal hover:text-white  gap-2">
              <FiEdit />
              <p>Edit</p>
            </div>
          </NavLink>
          <NavLink
            style={({ isActive }) => (isActive ? activeStyle : inactiveStyle)}
            to="/"
          >
            <div className="flex items-center font-normal hover:text-white  gap-2">
              <RiPagesLine />
              <p>Beranda</p>
            </div>
          </NavLink>
        </div>
      </div>
      <div className="font-extralight flex flex-col gap-6 mb-6">
        <NavLink
          style={({ isActive }) => (isActive ? activeStyle : inactiveStyle)}
          to="/dashboard/setting"
        >
          <div className="flex items-center font-normal hover:text-white  gap-2">
            <FiSettings className=" w-10 h-10 " />
            <p>Pengaturan</p>
          </div>
        </NavLink>
        <NavLink
          style={({ isActive }) => (isActive ? activeStyle : inactiveStyle)}
          to="/login"
        >
          <div className="flex items-center font-normal hover:text-white  gap-2">
            <FiLogOut />
            <p>Masuk</p>
          </div>
        </NavLink>
      </div>
    </div>
  );
};

export default Navbar;
