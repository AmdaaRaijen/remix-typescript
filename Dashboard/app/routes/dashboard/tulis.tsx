import { Form } from "@remix-run/react";
import { useEffect, useState } from "react";
import { ActionFunction } from "react-router";
import Body from "~/components/Content/Make/Body/Body";
import Tittle from "~/components/Content/Make/Tittle/Tittle";
import { v4 as uuidv4 } from "uuid";
import { redirect, createCookie } from "@remix-run/node";
import Cookies from "universal-cookie";
import parseCookie from "~/utils/parseCookie";
import UserType from "~/types/UserType";

const make = () => {
  const [user, setUser] = useState<string | null>(null);
  const cookie = new Cookies();

  useEffect(() => {
    const getUser = cookie.get("name");
    setUser(getUser);
    console.log(getUser);
    console.log(setUser);
  }, []);
  return (
    <div className="p-14">
      <h2 className="text-xl font-bold">Tulis Artikelmu</h2>
      <Form method="post">
        <Tittle />
        <Body />
        <button className="btn btn-primary">Buat</button>
      </Form>
    </div>
  );
};

export const action: ActionFunction = async ({ request }) => {
  const formData = await request.formData();
  const articleData = Object.fromEntries(formData.entries());
  const cookie = parseCookie(request.headers.get("cookie"));
  const articleFullData = {
    ...articleData,

    id: uuidv4(),
    user_id: cookie.id,
    date: new Date(),
  };

  const response = await fetch("http://localhost:5000/articles", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(articleFullData),
  });
  return redirect("/");
};

export default make;
