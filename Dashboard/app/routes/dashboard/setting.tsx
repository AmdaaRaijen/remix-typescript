import Cookies from "universal-cookie";
import Password from "~/components/Content/Setting/Password";
import PersonalData from "~/components/Content/Setting/PersonalData";
import { useEffect, useState } from "react";
import { redirect } from "@remix-run/node";

const setting = () => {
  const [check, setCheck] = useState<string | null>(null);
  function handleLogout(): void {
    const cookie = new Cookies({});
    cookie.remove("user");
    cookie.remove("name");
    console.log(cookie.remove("user", { path: "/" }));
    // redirect("/login");
  }

  useEffect(() => {
    const cookie = new Cookies();
    const user = cookie.get("user");
    setCheck(user);
  }, [check]);
  return (
    <div className="p-14">
      <h2 className="text-xl font-bold">Pengaturan</h2>
      <PersonalData />
      <Password />
      <div className="flex justify-between">
        <button className="btn bg-[#5A57FF] border-2 border-white mt-20">
          Simpan
        </button>
        <button
          onClick={handleLogout}
          className="btn bg-[#ff5757] border-2 border-white mt-20"
        >
          Keluar
        </button>
      </div>
    </div>
  );
};

export default setting;
