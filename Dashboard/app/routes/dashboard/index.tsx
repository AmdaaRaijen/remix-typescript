import Summary from "~/components/Content/Dashboard/Summary/Summary";
import Welcome from "~/components/Content/Dashboard/Welcome/Welcome";
import Card from "~/components/Content/Dashboard/Card/Card";
const dashboard = () => {
  return (
    <div className="p-14">
      <Welcome />
      <Card />
      <Summary />
    </div>
  );
};

export default dashboard;
