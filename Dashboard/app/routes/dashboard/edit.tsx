import React from "react";
import CardEdit from "~/components/Content/Edit/CardEdit";
import Edit from "~/components/Content/Edit/Edit";
import Search from "~/components/Content/Edit/Search";

const edit = () => {
  return (
    <div className="p-14">
      <h2 className="text-xl font-bold ">Edit Artikelmu</h2>
      <Search />
      <Edit />
    </div>
  );
};

export default edit;
