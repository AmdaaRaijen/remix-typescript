import { LoaderFunction, redirect } from "@remix-run/node";
import { Outlet } from "@remix-run/react";
import Cookies from "universal-cookie";
import Header from "~/components/Content/Header/Header";
import Sidebar from "~/components/Sidebar/Sidebar";

export const loader: LoaderFunction = async ({ request }) => {
  const cookie = new Cookies(request.headers.get("Cookie"));
  const user = cookie.get("user");
  const name = cookie.get("name");
  if (!user || !name) {
    return redirect("/login");
  }
  return {};
};

function dashboard() {
  return (
    <div className="flex min-h-screen h-[100vh] bg-[#faf9ff] font-poppins">
      <div className="z-50 sticky top-10 overflow-auto">
        <Sidebar />
      </div>
      <div className="w-full h-screen overflow-y-auto">
        <Header />
        <Outlet />
      </div>
    </div>
  );
}

export default dashboard;
