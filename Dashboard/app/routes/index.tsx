import Card from "~/components/Main/Body/Card/Card";
import Navbar from "~/components/Main/Navbar/Navbar";
import { useState, useEffect } from "react";
import ArticleType from "~/types/ArticleType";
import UserType from "~/types/UserType";
import Cookies from "universal-cookie";

export default function Index() {
  const [articles, setArticles] = useState<ArticleType[]>([]);

  async function handleFetchArticles() {
    const res = await fetch("http://localhost:5000/articles");
    const resUser = await fetch("http://localhost:5000/users");
    if (res.ok && resUser.ok) {
      const articlesJson = await res.json();
      const usersJson: UserType[] = await resUser.json();
      const articlesData: ArticleType[] = articlesJson.map((article: any) => {
        const author = usersJson.find((user) => user.id === article.user_id);
        return {
          ...article,
          author,
        };
      });
      setArticles(articlesData);
    }
  }

  const cookie = new Cookies();
  useEffect(() => {
    handleFetchArticles();
  }, []);
  return (
    <div className="min-h-screen bg-[#FAF9FF] font-poppins">
      <Navbar />
      <div className="flex justify-center items-center flex-col ">
        <div className="flex flex-col mt-5 gap-2 mb-5">
          {articles.map((article) => (
            <Card article={article} key={article.id} />
          ))}
        </div>
      </div>
    </div>
  );
}
