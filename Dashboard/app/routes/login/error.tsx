import React from "react";

const error = () => {
  return (
    <div className="w-screen h-screen flex justify-center items-center">
      <p className="text-3xl font-semibold">ERROR</p>
    </div>
  );
};

export default error;
