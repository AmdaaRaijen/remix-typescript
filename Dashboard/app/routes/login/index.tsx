import { ActionFunction, redirect } from "@remix-run/node";
import { Form, Link } from "@remix-run/react";
import { createCookie } from "@remix-run/node";
import UserType from "~/types/UserType";
import { useState } from "react";
import Cookies from "universal-cookie";

const login = () => {
  const [username, setUsername] = useState<string>("");
  const [password, setPassword] = useState<string>("");
  const [wrongPwd, setWrongPwd] = useState<boolean>(false);

  async function handleResponse() {
    const response = await fetch("http://localhost:5000/users", {
      method: "GET",
    });
    if (response.ok) {
      const users = await response.json();

      const theUserName = users.map((data: UserType) => data);
      function findName(theUserName: any) {
        return theUserName.username === username;
      }

      const theUser = theUserName.find(findName);
      const cookie = new Cookies();

      cookie.set("id", theUser.id, { path: "/" });
      cookie.set("user", theUser.username, { path: "/" });
      cookie.set("name", theUser.name, { path: "/" });

      if (!username || !password) {
        console.log("aisjd");
        setWrongPwd(true);
      } else if (theUser.password !== password) {
        console.log("salah");
        setWrongPwd(true);
      }
      try {
      } catch (error) {}
    }
  }

  return (
    <Form method="post">
      <div className="flex justify-center items-center h-[100vh] bg-[#FAF9FF] font-poppins">
        <div className="flex flex-col items-center justify-between w-[20rem] h-[26rem] bg-white rounded-xl shadow-md p-4">
          <div>
            <h2 className="font-semibold text-xl">Login</h2>
          </div>
          {/* =========  Data ========== */}
          {
            <>
              <div className="flex flex-col gap-5">
                <div className="flex flex-col justify-center items-center">
                  <label className="font-medium">Username / Email</label>
                  <input
                    type="text"
                    placeholder="Masukkan Username / Email"
                    className="text-center outline-none border-b-2"
                    name="username"
                    onChange={(e) => setUsername(e.target.value)}
                  />
                  {wrongPwd && (
                    <p className="text-red-500 opacity-50 mt-1">
                      Password atau Username salah
                    </p>
                  )}
                </div>
                <div className="flex flex-col justify-center items-center">
                  <label className="font-medium">Password</label>
                  <input
                    type="password"
                    placeholder="Masukkan password"
                    className="text-center outline-none border-b-2"
                    name="password"
                    onChange={(e) => setPassword(e.target.value)}
                  />
                  {wrongPwd && (
                    <p className="text-red-500 opacity-50 mt-1">
                      Password atau Username salah
                    </p>
                  )}
                </div>
              </div>
            </>
          }
          {/* =========  Button ========== */}
          <div className="w-full flex flex-col gap-4">
            <button
              onClick={handleResponse}
              className="btn w-full bg-[#5A57FF] border-none"
            >
              login
            </button>
            <div className="text-sm flex gap-2 justify-center">
              <p>belum punya akun?</p>
              <Link to="/login/daftar" className="text-[#5A57FF] underline">
                Daftar
              </Link>
            </div>
          </div>
        </div>
      </div>
    </Form>
  );
};

export const action: ActionFunction = async ({ request }) => {
  const response = await fetch("http://localhost:5000/users", {
    method: "GET",
  });

  if (response.ok) {
    const formData = await request.formData();
    const articleUserData = Object.fromEntries(formData.entries());
    const users = await response.json();

    const theUserName = users.map((data: UserType) => data);
    function findName(theUserName: any) {
      return theUserName.username === articleUserData.username;
    }

    const theUser = theUserName.find(findName);

    if (!articleUserData.username || !articleUserData.password || !theUser) {
      return redirect("/login");
    } else if (
      theUser.password === articleUserData.password &&
      theUser.username === articleUserData.username
    ) {
      return redirect("/dashboard");
    }

    return redirect("/login");
  }
  return redirect("/login");
};

export default login;
