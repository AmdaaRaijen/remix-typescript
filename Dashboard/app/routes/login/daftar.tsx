import { ActionFunction, redirect } from "@remix-run/node";
import { Form, Link } from "@remix-run/react";
import { v4 as uuidv4 } from "uuid";
import { db } from "../../../db.server";
import UserType from "~/types/UserType";
const daftar = () => {
  return (
    <Form method="post">
      <div className="flex justify-center items-center h-[100vh] bg-[#FAF9FF] font-poppins">
        <div className="flex flex-col items-center justify-between w-[20rem] h-[26rem] bg-white rounded-xl shadow-md p-4">
          <div>
            <h2 className="font-semibold text-xl">Daftar</h2>
          </div>
          {/* =========  Data ========== */}
          <div className="flex flex-col gap-5">
            <div className="flex flex-col justify-center items-center">
              <label className="font-medium">Nama</label>
              <input
                type="text"
                placeholder="Masukkan Nama"
                className="text-center outline-none border-b-2"
                name="name"
                maxLength={25}
                minLength={1}
              />
            </div>
            <div className="flex flex-col justify-center items-center">
              <label className="font-medium">Username / Email</label>
              <input
                type="text"
                placeholder="Masukkan Username / Email"
                className="text-center outline-none border-b-2"
                name="username"
                maxLength={15}
                minLength={1}
              />
            </div>
            <div className="flex flex-col justify-center items-center">
              <label className="font-medium">Password</label>
              <input
                type="password"
                placeholder="Masukkan password"
                className="text-center outline-none border-b-2"
                name="password"
                minLength={6}
              />
            </div>
          </div>
          {/* =========  Button ========== */}
          <div className="w-full flex flex-col gap-4">
            <button className="btn w-full bg-[#5A57FF] border-none">
              daftar
            </button>
            <div className="text-sm flex gap-2 justify-center">
              <p>Sudah punya akun?</p>
              <Link to="/login" className="text-[#5A57FF] underline">
                Login
              </Link>
            </div>
          </div>
        </div>
      </div>
    </Form>
  );
};

export const action: ActionFunction = async ({ request }) => {
  const formData = await request.formData();
  const articleData = Object.fromEntries(formData.entries());
  const articleFullData = {
    id: uuidv4(),
    name: articleData.name,
    username: articleData.username,
    password: articleData.password,
  };

  if (
    articleFullData.name === "" ||
    articleFullData.password === "" ||
    articleFullData.username === ""
  ) {
    return redirect("/login/error");
  }
  const response = await fetch("http://localhost:5000/users", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(articleFullData),
  });
  return redirect("/login");
};

export default daftar;
