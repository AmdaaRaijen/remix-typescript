export default function parseCookie(cookie: string | null) {
  if (!cookie) return {};
  const cookies = cookie.split(";");
  const parsedCookies: { [key: string]: string } = {};
  cookies.forEach((cookie) => {
    const [key, value] = cookie.split("=");
    parsedCookies[key.trim()] = value;
  });
  return parsedCookies;
}
