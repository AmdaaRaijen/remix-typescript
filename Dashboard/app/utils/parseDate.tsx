function parseDate(dateString: string) {
  const date: any = new Date(dateString);
  const now: any = new Date();
  const diffInMs = now - date;
  const diffInDays = diffInMs / (1000 * 60 * 60 * 24);

  let result;
  if (diffInDays < 7) {
    result = Math.floor(diffInDays) + " hari yang lalu";
  } else if (diffInDays < 14) {
    result = "1 minggu yang lalu";
  } else {
    result = new Intl.DateTimeFormat("id-ID", {
      weekday: "long",
      day: "numeric",
      month: "long",
      year: "numeric",
    }).format(date);
  }

  return result;
}

export default parseDate;
