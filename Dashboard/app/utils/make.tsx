import fs from "fs/promises";

export async function getStoredArticle() {
  const rawArticles: string = await fs.readFile("db.json", "utf-8");
  const data = JSON.parse(rawArticles);
  const storedArticles = data.articles ?? [];
  return storedArticles;
}

export async function storeArticle(articles: any) {
  return await fs.writeFile(
    "db.json",
    JSON.stringify({ articles: articles || [] })
  );
}
