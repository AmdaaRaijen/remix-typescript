// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import {
  applicationDefault,
  initializeApp as initializeAdminApp,
} from "firebase-admin/app";
import { database } from "firebase-admin";

import { getAuth } from "firebase/auth";

require("dotenv").config();
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyDgieWj3eQOf_YTgo16tquMEeLApLMXZBU",
  authDomain: "article-builder-94c74.firebaseapp.com",
  projectId: "article-builder-94c74",
  storageBucket: "article-builder-94c74.appspot.com",
  messagingSenderId: "891011606733",
  appId: "1:891011606733:web:f0ab2ae5e1461cc08b6184",
  measurementId: "G-J38CXSHLP6",
};

if (!admin.apps.length) {
  initializeAdminApp({
    credential: applicationDefault(),
    databaseURL: "https://article-builder-94c74.asia-southeast2.firebaseio.com",
  });
}

const db = admin.firestore();

let Firebase;

if (!Firebase?.app?.length) {
  Firebase = initializeApp(firebaseConfig);
}

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const auth = getAuth(app);

export { db };
