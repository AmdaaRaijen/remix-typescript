import { Link } from "@remix-run/react";

export default function Index() {
  return (
    <div className="h-[100vh] flex justify-center items-center">
      <div>
        <h1 className="text-xl font-bold">Hello REMIX ...</h1>
        <Link to="/page">Click Me...</Link>
      </div>
    </div>
  );
}
