import { Link } from "@remix-run/react";

const page = () => {
  return (
    <div className="h-[100vh] flex justify-center items-center flex-col">
      <h1 className="font-bold text-xl">This Is Completely Different Page</h1>
      <Link to="/">Back</Link>
    </div>
  );
};

export default page;
