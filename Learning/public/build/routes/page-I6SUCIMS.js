import {
  __toESM,
  require_jsx_dev_runtime
} from "/build/_shared/chunk-EETRBLDB.js";

// app/routes/page.tsx
var import_jsx_dev_runtime = __toESM(require_jsx_dev_runtime());
var page = () => {
  return /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("div", { children: /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("h1", { children: "This Is Completely Different Page" }, void 0, false, {
    fileName: "app/routes/page.tsx",
    lineNumber: 4,
    columnNumber: 7
  }, this) }, void 0, false, {
    fileName: "app/routes/page.tsx",
    lineNumber: 3,
    columnNumber: 5
  }, this);
};
var page_default = page;
export {
  page_default as default
};
//# sourceMappingURL=/build/routes/page-I6SUCIMS.js.map
