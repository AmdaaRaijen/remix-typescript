import {
  Link,
  __toESM,
  require_jsx_dev_runtime
} from "/build/_shared/chunk-HEAZXX2O.js";

// app/routes/page.tsx
var import_jsx_dev_runtime = __toESM(require_jsx_dev_runtime());
var page = () => {
  return /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("div", { className: "h-[100vh] flex justify-center items-center flex-col", children: [
    /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("h1", { className: "font-bold text-xl", children: "This Is Completely Different Page" }, void 0, false, {
      fileName: "app/routes/page.tsx",
      lineNumber: 6,
      columnNumber: 7
    }, this),
    /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)(Link, { to: "/", children: "Back" }, void 0, false, {
      fileName: "app/routes/page.tsx",
      lineNumber: 7,
      columnNumber: 7
    }, this)
  ] }, void 0, true, {
    fileName: "app/routes/page.tsx",
    lineNumber: 5,
    columnNumber: 5
  }, this);
};
var page_default = page;
export {
  page_default as default
};
//# sourceMappingURL=/build/routes/page-OUKBRJ2Z.js.map
