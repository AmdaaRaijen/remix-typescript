import { Link } from "@remix-run/react";
import Card from "~/components/Card";

const app = () => {
  return (
    <div>
      <Link to="/">
        <p className="absolute top-2 left-2 font-semibold ">Back</p>
      </Link>
      <div className="h-[100vh] flex justify-center items-center flex-col ">
        <Card />
      </div>
      <div></div>
    </div>
  );
};

export default app;
