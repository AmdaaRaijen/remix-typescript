import { Link } from "@remix-run/react";

export default function Index() {
  return (
    <div className="h-[100vh] flex justify-center items-center flex-col gap-10">
      <div className="text-center">
        <h1 className="font-bold text-9xl font-arial">Many Apps</h1>
        <p className="mt-4 text-2xl">
          This is website of my app that i have ever made
        </p>
      </div>
      <div>
        <Link to="/manyApp">
          <p className="p-3 border-2 rounded-md shadow-md hover:shadow-none ease-in duration-150">
            let's go
          </p>
        </Link>
      </div>
    </div>
  );
}
